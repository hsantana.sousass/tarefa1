# Tarefa
- Clone o repositório com o roteiro dos comandos aprendidos na aula:
- Faça um mini resumo de cada comando aprendido, você pode consultar o google ou os outros Trainees
- Usem commit para as alterações que vocês fizerem
- Depois que o resumo estiver pronto, crie um repositório seu no GitLab e envie o repositório local para lá (Lembre-se que o repositório precisa ser público e também de mudar o endereço remoto)

Comandos:

git init -> comando para iniciar o git na pasta

git add arquivo/diretório -> Adiciona o arquivo/diretório que foram modificados para serem comitados.

git add . ->  Adiciona tudo que foi modificados para serem comitados.

git status -> Mostra a situação dos arquivos, se foram modificados ou não.

git commit -m "Mensagem do commit" -> Depois de ser feito uma mudança e ter usado o git add, é usado o git commit para mostrar o que foi alterado no código.

git log -> mostra todos os commits.

git checkout nomeDaBranch  -> Navega entre as Branch.

git checkout -b nomeDaBranch -> Cria uma Branch da Branch atual.

git merge nomeDaBranch -> Traz todas as mundanças da branch especificada para a branch atual.

git push -> Manda os arquivos da pasta do seu projeto para o repositório

git pull -> Faz o oposto do push

git clone -> Clona um repositório na pasto do seu projeto